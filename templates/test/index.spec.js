const expect = require('chai').expect;
const lambda = require('../release/index');
const event = require('./fixtures/event.json');

describe('Lambda invokation', () => {

    describe('handles correct event content by', () => {
        it('returning the correct "Hello there!"', done => {
            lambda.handler(event, {}, (error, result) => {
                expect(error).to.equal(null);

                expect(result).to.be.a('object');
                expect(result.statusCode).to.equal(200);
                expect(result.body).to.equal('Hello there!');

                done();
            })
        })
    })

    describe('handles incorrect event content by', () => {
        it('returning a correct status code and an error', done => {
            lambda.handler({}, {}, (error, result) => {
                expect(error).to.equal("Something went wrong!");

                expect(result).to.be.a('object');
                expect(result.statusCode).to.equal(500);

                done();
            })
        })
    })
});