import { Context, Callback, APIGatewayEvent } from "aws-lambda";

exports.handler = (
  event: APIGatewayEvent,
  context: Context,
  callback: Callback
) => {
  console.log(`Debug: event received: ${JSON.stringify(event)}`);

  const body: string = `Hello ${event.body ? event.body : "World"}!`;
  const statusCode: number = event.httpMethod === "POST" ? 200 : 500;

  new Promise((resolve, reject) => statusCode === 200 ? resolve() : reject("Something went wrong!"))
    .then((_) => callback(null, { statusCode, body }))
    .catch((error) => callback(error, { statusCode }));
};