# Create Node.js Project
This is a scaffolding project to be able to create Node.js projects following NBCUniversal standards.

# Documentation
Available here: https://confluence.globant.com/display/COM013/Node.js+scaffolding

### Create new project

> `npm init bitbucket:leoglobant/glb-nodejs-package new-package`

This command will create a new folder `new-package` following this repository file structure to scaffold a Node.js type of project.

### Credits
- Based on the initialization project from https://github.com/kevinpollet/create-node-typescript